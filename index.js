
function $q(input) {
    return document.querySelector(input);
}

function $qa(input) {
    return document.querySelectorAll(input);
}

function invElement(element, setTimeOutVal = 1000, invisible = "invisible", invisibleAgain = "invisibleAgain") {

    setTimeout(() => {

        element.classList.remove("visible");
        element.classList.remove("visibleAgain");
        element.classList.add(invisible);
        setTimeout(() => {
            element.classList.add(invisibleAgain);
            element.style.display = "none";
        }, 1100);
    }, setTimeOutVal);
}

function visElement(element, vis = "visible", visAg = "visibleAgain") {
    element.classList.remove("invisible");
    element.classList.remove("invisibleAgain");

    element.classList.add("visible");
    element.classList.add("visibleAgain");
    element.style.display = "block";

    console.log(element.classList)

}

(function () {
    const textElements = $qa(".😎");
    const intro = $q(".🥺");
    const wrap = $q(".wrapper");
    let status = false;
    textElements.forEach((element, index, array) => {
        if (status == false) {
            array[index + 1].style.display = "none";
            invElement(element);
            status = true;
            // continue;
        } else {
            setTimeout(() => {
                console.log("element kedua!!!", element)
                visElement(element, 3000);
                invElement(element, 3000);
                // invElement(element);
                setTimeout(() => {
                    intro.style.display = "none"
                    wrap.classList.add("visible")
                }, 5000);

            }, 2100);
        }
    })
})();
